<?php

require 'connect.php';

// get the posted data
$postdata = file_get_contents("php://input");


if(isset($postdata) && !empty($postdata)) {
    // extract the data
    $request = json_decode($postdata);
    
    // validate
    if(trim($request->data->model) == '' || (int)$request->data->price <1) {
        return http_response_code(400);
    }


    // sanitize (assainit)
    $model = mysqli_real_escape_string($con, trim($request->data->model));
    $price = mysqli_real_escape_string($con, (int)$request->data->price);

    // Store
    $sql = "INSERT INTO cars (id,model,price) VALUES (null,?,?);";
    $stmt = $con->prepare($sql);
    $stmt->bind_param('si', $model, $price);
    
    if($stmt->execute()) {
        http_response_code(201);
        $car = [
            'model' => $model,
            'price' => $price,
            'id' => mysqli_insert_id($con)
        ];
        echo json_encode(['data'=>$car]);
        

    } else {
        http_response_code(422);
    }

}


?>