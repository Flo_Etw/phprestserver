<?php

require 'connect.php';

// extract, validate & sanitize the id
$id = ($_GET['id'] !== null && (int)$_GET['id'] > 0)? mysqli_real_escape_string($con, (int)$_GET['id']) : false;

if(!$id) {
    return http_response_code(400);
}

// delete
$sql = "DELETE FROM cars WHERE id = ? LIMIT 1;";

$stmt = $con->prepare($sql);
$stmt->bind_param('i', $id);

if($stmt->execute()) {
    http_response_code(204);
} else {
    return http_response_code(422);
}


?>