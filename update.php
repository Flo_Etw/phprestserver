<?php

require 'connect.php';

// get the posted data
$postdata = file_get_contents("php://input");

if(isset($postdata) && !empty($postdata)) {

    // extract the data
    $request = json_decode($postdata);

    // validate
    if((int)$request->data->id < 1 || trim($request->data->model) === '' || (int)$request->data->price < 1) {
        return http_response_code(400);
    }

    // sanitize
    $id = mysqli_real_escape_string($con, (int)$request->data->id);
    $model = mysqli_real_escape_string($con, trim($request->data->model));
    $price = mysqli_real_escape_string($con, (int)$request->data->price);

    // update
    $sql = "UPDATE cars SET model = ?, price = ? WHERE id= ? LIMIT 1;";

    $stmt = $con->prepare($sql);
    $stmt->bind_param('sii', $model, $price, $id);
    
    if($stmt->execute()) {
        http_response_code(204);

    } else {
        http_response_code(422);
    }

}

?>